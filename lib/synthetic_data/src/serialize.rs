use uuid::Uuid;

pub fn uuid_vec<S>(id_vec: &[Uuid], serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    let s_vec = id_vec
        .iter()
        .map(|id| id.to_string())
        .collect::<Vec<String>>();
    serde::Serialize::serialize(&s_vec, serializer)
}
