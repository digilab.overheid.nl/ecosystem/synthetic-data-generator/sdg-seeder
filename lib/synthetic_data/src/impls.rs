use crate::{constants::*, types::*};
use fake::{faker::automotive::fr_fr::LicencePlate, Dummy, Fake, Faker, StringFaker};
use rand::{prelude::SliceRandom, Rng};

impl Dummy<Faker> for PlotNumber {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        PlotNumber((1..=99999).fake_with_rng(rng))
    }
}

impl Dummy<Faker> for Municipality {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        let (code, name, _) = MUNICIPALITIES.choose_weighted(rng, |item| item.2).unwrap(); // Note: child birth numbers are used as weight for municipality choice. IMPROVE: use other statics (e.g. population size or area surface, depending on the usage)
        Municipality { code, name }
    }
}

impl Dummy<Faker> for Section {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        Section(
            StringFaker::with(Vec::from("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 2)
                .fake_with_rng::<String, R>(rng),
        )
    }
}

impl Dummy<Faker> for Street {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        Street(STREET_NAMES.choose(rng).unwrap())
    }
}

impl Dummy<Faker> for HouseNumber {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        HouseNumber((1..=100).fake_with_rng(rng)) // IMPROVE: use another distribution, or determine the maximum number randomly according to a normal distribution of number of house numbers per street. See also https://en.wikipedia.org/wiki/Benford%27s_law (which relates only to the first digit)
    }
}

impl Dummy<Faker> for HouseLetter {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        // Generate a random number between 0 and 1
        let random_number: f64 = rng.gen();

        // In 70.6% of all cases, return an empty house letter, see constants.rs for details
        if random_number < 0.706 {
            return HouseLetter("".to_string());
        }

        // Else: randomly select a house letter
        let (letter, _) = HOUSE_LETTERS.choose_weighted(rng, |item| item.1).unwrap();
        HouseLetter(letter.to_string())
    }
}

impl Dummy<Faker> for HouseNumberAddition {
    // Note: according to https://www.kadaster.nl/-/kosteloze-download-bag-2-0-extract, ~11.4% of house numbers has an addition. For simplicity, we assume all additions have equal probability to occur
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        // Generate a random number between 0 and 1
        let random_number: f64 = rng.gen();

        if random_number > 0.114 {
            return HouseNumberAddition("");
        }

        HouseNumberAddition(HOUSE_NUMBER_ADDITIONS.choose(rng).unwrap())
    }
}

const UPPER: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

impl Dummy<Faker> for PostalCode {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        let code: u64 = (1000..10000).fake_with_rng(rng);
        PostalCode(format!(
            "{code}{}",
            StringFaker::with(Vec::from(UPPER), 2).fake_with_rng::<String, R>(rng)
        ))
    }
}

impl Dummy<Faker> for Purpose {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        Purpose(PURPOSES.choose(rng).unwrap())
    }
}

impl Dummy<Faker> for Surface {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        Surface((20..=200).fake_with_rng(rng))
    }
}

impl Dummy<Faker> for Registrees {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        Registrees((1..=5).fake_with_rng(rng))
    }
}

impl Dummy<Faker> for Rsin {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        Rsin((100000000..=999999999).fake_with_rng(rng))
    }
}

impl Dummy<Faker> for Gender {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        let (gender, _) = GENDERS.choose_weighted(rng, |item| item.1).unwrap();
        Gender(gender)
    }
}

impl From<&Gender> for &str {
    fn from(gender: &Gender) -> Self {
        gender.0
    }
}

impl FirstName {
    pub fn new<R: Rng + ?Sized>(gender: &Gender, rng: &mut R) -> Self {
        match gender.into() {
            "F" => FirstName(FIRST_NAMES_FEMALE.choose(rng).unwrap()),
            "M" => FirstName(FIRST_NAMES_MALE.choose(rng).unwrap()),
            "X" => FirstName(FIRST_NAMES_X.choose(rng).unwrap()),
            _ => unimplemented!(),
        }
    }
}

impl Dummy<Faker> for FamilyNameWithPrefix {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        let (prefix, family_name, _) = FAMILY_NAMES.choose_weighted(rng, |item| item.2).unwrap();
        FamilyNameWithPrefix(Prefix(prefix), FamilyName(family_name))
    }
}

impl Dummy<Faker> for CarModel {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        CarModel(CAR_MODELS.choose(rng).unwrap())
    }
}

impl Dummy<Faker> for NumberPlate {
    fn dummy_with_rng<R: Rng + ?Sized>(_: &Faker, rng: &mut R) -> Self {
        NumberPlate(LicencePlate().fake_with_rng(rng)) // format matches the one used in NL
    }
}
