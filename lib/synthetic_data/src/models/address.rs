use fake::{Dummy, Fake, Faker};
use rand::Rng;
use serde::Serialize;
use uuid::Uuid;

use crate::{event::Event, types::*};

#[derive(Debug, Dummy, Serialize)]
pub struct Registration {
    pub street: Street,
    #[serde(rename = "houseNumber")]
    pub house_number: HouseNumber, // E.g. 1 or 123
    #[serde(rename = "houseLetter")]
    pub house_letter: HouseLetter, // E.g. `a` or `B`. Note: only one letter is allowed, see https://imbag.github.io/catalogus/hoofdstukken/attributen--relaties#733-huisletter
    #[serde(rename = "houseNumberAddition")]
    pub house_number_addition: HouseNumberAddition, // E.g. bis. Note: 1-4 characters, see https://imbag.github.io/catalogus/hoofdstukken/attributen--relaties#734-huisnummertoevoeging
    #[serde(rename = "postalCode")]
    pub postal_code: PostalCode,
    pub municipality: Municipality,
    pub purpose: Purpose,
    pub surface: Surface,
}

pub struct Address {
    pub id: Uuid,
    pub registration: Registration,
}

impl Address {
    pub fn new<R: Rng + ?Sized>(rng: &mut R) -> Self {
        Self {
            id: Uuid::new_v4(),
            registration: Faker.fake_with_rng::<Registration, R>(rng),
        }
    }

    pub fn to_events<R: Rng>(&self, rng: &mut R) -> Vec<Event> {
        vec![Event::new(
            &self.registration,
            "AdresGeregistreerd".to_string(),
            None,
            self.id,
            rng,
        )]
    }
}
