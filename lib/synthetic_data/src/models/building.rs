use chrono::{DateTime, Duration, Utc};
use fake::{faker::chrono::en::DateTimeBetween, Dummy, Fake, Faker};
use math::round;
use rand::{prelude::SliceRandom, Rng};
use serde::Serialize;
use uuid::Uuid;

use crate::{event::Event, models::Person, types::*};

#[derive(Debug, Serialize)]
pub struct AddressAssignment {
    pub address: Uuid,
}

#[derive(Debug, Serialize)]
pub struct Registration {
    #[serde(rename = "constructedAt")]
    pub constructed_at: DateTime<Utc>,
    pub surface: Surface,
}

impl Registration {
    fn new<R: Rng + ?Sized>(inception: DateTime<Utc>, rng: &mut R) -> Self {
        Self {
            constructed_at: DateTimeBetween(inception, Utc::now() - Duration::days(32))
                .fake_with_rng(rng),
            surface: Faker.fake_with_rng::<Surface, R>(rng),
        }
    }
}

#[derive(Debug, Dummy, Serialize)]
pub enum Type {
    Residential,
    NonResidential,
}

#[derive(Debug, Serialize)]
pub struct Valuation {
    pub value: u64,
    #[serde(rename = "valuationAt")]
    pub valuation_at: DateTime<Utc>,
    #[serde(rename = "effectiveAt")]
    pub effective_at: DateTime<Utc>,
}

#[allow(dead_code)]
#[derive(Debug, Serialize)]
pub enum Subject {
    BSN(BSN),
    Rsin(Rsin),
}

#[derive(Debug, Serialize)]
pub struct ValueDetermination {
    pub id: Uuid,
    #[serde(rename = "addressableObjectId")]
    pub addressable_object_id: Uuid,
    pub owner: Subject,
    pub occupant: Option<Subject>,
    #[serde(rename = "registeredPersons")]
    pub registered_persons: Registrees,
    #[serde(rename = "type")]
    pub type_: Type,
    pub values: Vec<Valuation>,
    // pub municipality: Municipality,
}

impl ValueDetermination {
    fn new<R: Rng + ?Sized>(
        addressable_object_id: Uuid,
        owner: &Person,
        persons: &Vec<Person>,
        rng: &mut R,
    ) -> Self {
        Self {
            id: Uuid::new_v4(),
            addressable_object_id,
            owner: Subject::BSN(owner.bsn),
            occupant: Self::occupant(persons, rng),
            registered_persons: Faker.fake_with_rng::<Registrees, R>(rng),
            type_: Type::Residential,
            values: vec![Valuation {
                value: round::ceil((100000.0..=1000000.0).fake_with_rng::<f64, R>(rng), -3) as u64,
                valuation_at: DateTimeBetween(Utc::now() - Duration::days(6), Utc::now())
                    .fake_with_rng(rng),
                effective_at: DateTimeBetween(Utc::now() - Duration::days(8), Utc::now())
                    .fake_with_rng(rng),
            }],
            // municipality: owner.address_registration.municipality,
        }
    }

    fn occupant<R: Rng + ?Sized>(persons: &Vec<Person>, rng: &mut R) -> Option<Subject> {
        if rng.gen::<bool>() {
            let occupant = persons.choose(rng).unwrap();
            Some(Subject::BSN(occupant.bsn))
        } else {
            None
        }
    }
}

pub struct Building {
    pub id: Uuid,
    pub registration: Registration,
    pub address_assignment: AddressAssignment,
    pub value_determination: ValueDetermination,
    inception: DateTime<Utc>,
}

impl Building {
    pub fn new<R: Rng + ?Sized>(
        inception: DateTime<Utc>,
        address: Uuid,
        owner: &Person,
        persons: &Vec<Person>,
        rng: &mut R,
    ) -> Self {
        let registration = Registration::new(inception, rng);
        let address_assignment = AddressAssignment { address };
        let value_determination = ValueDetermination::new(address, owner, persons, rng);

        Self {
            id: Uuid::new_v4(),
            registration,
            address_assignment,
            value_determination,
            inception,
        }
    }

    pub fn to_events<R: Rng>(&self, rng: &mut R) -> Vec<Event> {
        let registration_event = self.registration_event(rng);
        let address_registration_event =
            self.address_assignment_event(registration_event.registered_at, rng);
        let value_determination_event =
            self.value_determination_event(address_registration_event.registered_at, rng);
        vec![
            value_determination_event,
            address_registration_event,
            registration_event,
        ]
    }

    pub fn registration_event<R: Rng>(&self, rng: &mut R) -> Event {
        Event::new(
            &self.registration,
            "GebouwGeregistreerd".to_string(),
            Some(
                DateTimeBetween(self.inception, Utc::now() - Duration::days(32)).fake_with_rng(rng),
            ),
            self.id,
            rng,
        )
    }

    pub fn address_assignment_event<R: Rng>(&self, inception: DateTime<Utc>, rng: &mut R) -> Event {
        Event::new(
            &self.address_assignment,
            "AdresToewijzing".to_string(),
            Some(DateTimeBetween(inception, inception + Duration::days(16)).fake_with_rng(rng)),
            self.id,
            rng,
        )
    }

    pub fn value_determination_event<R: Rng>(
        &self,
        inception: DateTime<Utc>,
        rng: &mut R,
    ) -> Event {
        Event::new(
            &self.value_determination,
            "WozBepaling".to_string(),
            Some(DateTimeBetween(inception, inception + Duration::days(8)).fake_with_rng(rng)),
            self.id,
            rng,
        )
    }
}
