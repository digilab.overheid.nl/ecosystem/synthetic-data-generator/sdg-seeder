use fake::Fake;
use rand::Rng;
use std::collections::HashSet;

#[derive(Default)]
pub struct BsnState {
    used: HashSet<u32>,
}

// is_valid_bsn returns whether or not the specified integer is a valid BSN
// Note: for slight performance benefits, the function assumes that the specified candidate already has the correct number of digits (8 or 9)
fn is_valid_bsn(candidate: u32) -> bool {
    let mut num = candidate;
    let mut sum: i32 = 0;

    // Loop over all digits, beginning with the rightmost digit. Note: this approach seems faster than converting the number to string, extracting the digits and converting them to integers again
    let mut i: i32 = 1;
    while num != 0 {
        let digit = (num % 10) as i32;

        // Add the digits to the sum, multiplying by 1..9
        if i == 1 {
            sum = -digit; // IMPROVE: store this value and subtract it later, so we can use u32 instead of i32? Then first check if the cadidate is >= 10 (or actually, >= 10000000)
        } else {
            sum += i * digit;
        }

        num /= 10; // Note: the result will be floored/truncated, so no need to subtract `digit` from `num` first
        i += 1;
    }

    sum % 11 == 0
}

impl BsnState {
    pub fn generate_new<R: Rng + ?Sized>(&mut self, rng: &mut R) -> u32 {
        // Generate random candidates until a valid unused candidate is found
        let mut candidate = (10000008..=999999990).fake_with_rng(rng); // Note: 10000008 is the lowest valid 8-digit BSN and 999999990 is the highest 9-digit BSN
        while !is_valid_bsn(candidate) || !self.used.insert(candidate) {
            candidate = (10000008..=999999990).fake_with_rng(rng);
        }
        candidate
    }

    pub fn mark_as_used(&mut self, bsn: u32) {
        self.used.insert(bsn);
    }
}
