#[derive(Clone)]
pub struct Config {
    pub num_instances: usize,
    pub seed: Option<String>,
}
