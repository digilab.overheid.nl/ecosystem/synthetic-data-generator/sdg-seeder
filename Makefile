.PHONY: build
build:
	cargo build -p seeder

.PHONY: run
run:
	cargo run -p seeder

# cargo install cargo-watch
.PHONY: watch
watch:
	cargo watch -x 'run --color=always -p seeder 2>&1 | less'

.PHONY: container
CRI:=$(shell type -p podman || echo docker)
container:
	$(CRI) build --net=host -f Dockerfile -t  registry.int.tooling.digilab.network/digilab.overheid.nl/ecosystem/synthetic-data-generator/seeder:latest .

.PHONY: check
check: udeps clippy

# Tooling
## cargo install cargo-udeps
.PHONY: udeps
udeps:
	cargo +nightly udeps

.PHONY: clippy
clippy:
	cargo clippy
